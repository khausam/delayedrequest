/*
This file is part of DelayedRequest.

DelayedRequest is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

DelayedRequest is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with DelayedRequest. If not, see <https://www.gnu.org/licenses/>.
 */
package com.vaxem.delayedrequest;

import com.google.cloud.tasks.v2.CloudTasksClient;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteDelayedServlet extends HttpServlet {
    private static final String ID_PARAM = "id";

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        try {
            String idParam = request.getParameter(ID_PARAM);
            if(idParam != null && !idParam.isEmpty()) {
                if(idParam.matches("[0-9]+")) {
                    CloudTasksClient client = CloudTasksClient.create();
                    client.deleteTask(CreateDelayedServlet.QUEUE_PATH + "/tasks/" + idParam);

                    response.getWriter().println("Delete operation successfully attempted for id: " + idParam);
                }
                else {
                    response.getWriter().println("Parameter 'id' is formatted incorrectly. [Format: only digits]");
                }
            }
            else {
                response.getWriter().println("Required parameter 'id' is missing. [Description: the numeric request id returned from the create call.]");
            }
        }
        catch(RuntimeException rex) {
            response.getWriter().println("Error: " + rex.getLocalizedMessage());
        }
    }
}