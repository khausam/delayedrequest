/*
This file is part of DelayedRequest.

DelayedRequest is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

DelayedRequest is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with DelayedRequest. If not, see <https://www.gnu.org/licenses/>.
 */
package com.vaxem.delayedrequest;

import com.google.cloud.tasks.v2.HttpRequest;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.*;

import com.google.cloud.tasks.v2.QueueName;

public abstract class AbstractProxyServlet extends HttpServlet {


    protected static final Set<String> HOP_HEADERS;
    static {
        Set<String> hopHeaders = new HashSet<>();
        hopHeaders.add("connection");
        hopHeaders.add("keep-alive");
        hopHeaders.add("proxy-authorization");
        hopHeaders.add("proxy-authenticate");
        hopHeaders.add("proxy-connection");
        hopHeaders.add("transfer-encoding");
        hopHeaders.add("te");
        hopHeaders.add("trailer");
        hopHeaders.add("upgrade");
        HOP_HEADERS = Collections.unmodifiableSet(hopHeaders);
    }

    private boolean _preserveHost;
    private String _hostHeader;
    private String _viaHost;

    public enum HttpHeader {
        /**
         * General Fields.
         */
        CONNECTION("Connection"),
        CACHE_CONTROL("Cache-Control"),
        DATE("Date"),
        PRAGMA("Pragma"),
        PROXY_CONNECTION("Proxy-Connection"),
        TRAILER("Trailer"),
        TRANSFER_ENCODING("Transfer-Encoding"),
        UPGRADE("Upgrade"),
        VIA("Via"),
        WARNING("Warning"),
        NEGOTIATE("Negotiate"),

        /**
         * Entity Fields.
         */
        ALLOW("Allow"),
        CONTENT_ENCODING("Content-Encoding"),
        CONTENT_LANGUAGE("Content-Language"),
        CONTENT_LENGTH("Content-Length"),
        CONTENT_LOCATION("Content-Location"),
        CONTENT_MD5("Content-MD5"),
        CONTENT_RANGE("Content-Range"),
        CONTENT_TYPE("Content-Type"),
        EXPIRES("Expires"),
        LAST_MODIFIED("Last-Modified"),

        /**
         * Request Fields.
         */
        ACCEPT("Accept"),
        ACCEPT_CHARSET("Accept-Charset"),
        ACCEPT_ENCODING("Accept-Encoding"),
        ACCEPT_LANGUAGE("Accept-Language"),
        AUTHORIZATION("Authorization"),
        EXPECT("Expect"),
        FORWARDED("Forwarded"),
        FROM("From"),
        HOST("Host"),
        IF_MATCH("If-Match"),
        IF_MODIFIED_SINCE("If-Modified-Since"),
        IF_NONE_MATCH("If-None-Match"),
        IF_RANGE("If-Range"),
        IF_UNMODIFIED_SINCE("If-Unmodified-Since"),
        KEEP_ALIVE("Keep-Alive"),
        MAX_FORWARDS("Max-Forwards"),
        PROXY_AUTHORIZATION("Proxy-Authorization"),
        RANGE("Range"),
        REQUEST_RANGE("Request-Range"),
        REFERER("Referer"),
        TE("TE"),
        USER_AGENT("User-Agent"),
        X_FORWARDED_FOR("X-Forwarded-For"),
        X_FORWARDED_PORT("X-Forwarded-Port"),
        X_FORWARDED_PROTO("X-Forwarded-Proto"),
        X_FORWARDED_SERVER("X-Forwarded-Server"),
        X_FORWARDED_HOST("X-Forwarded-Host"),

        /**
         * Response Fields.
         */
        ACCEPT_RANGES("Accept-Ranges"),
        AGE("Age"),
        ETAG("ETag"),
        LOCATION("Location"),
        PROXY_AUTHENTICATE("Proxy-Authenticate"),
        RETRY_AFTER("Retry-After"),
        SERVER("Server"),
        SERVLET_ENGINE("Servlet-Engine"),
        VARY("Vary"),
        WWW_AUTHENTICATE("WWW-Authenticate"),

        /**
         * WebSocket Fields.
         */
        ORIGIN("Origin"),
        SEC_WEBSOCKET_KEY("Sec-WebSocket-Key"),
        SEC_WEBSOCKET_VERSION("Sec-WebSocket-Version"),
        SEC_WEBSOCKET_EXTENSIONS("Sec-WebSocket-Extensions"),
        SEC_WEBSOCKET_SUBPROTOCOL("Sec-WebSocket-Protocol"),
        SEC_WEBSOCKET_ACCEPT("Sec-WebSocket-Accept"),

        /**
         * Other Fields.
         */
        COOKIE("Cookie"),
        SET_COOKIE("Set-Cookie"),
        SET_COOKIE2("Set-Cookie2"),
        MIME_VERSION("MIME-Version"),
        IDENTITY("identity"),

        X_POWERED_BY("X-Powered-By"),
        HTTP2_SETTINGS("HTTP2-Settings"),

        STRICT_TRANSPORT_SECURITY("Strict-Transport-Security"),

        /**
         * HTTP2 Fields.
         */
        C_METHOD(":method"),
        C_SCHEME(":scheme"),
        C_AUTHORITY(":authority"),
        C_PATH(":path"),
        C_STATUS(":status"),

        UNKNOWN("::UNKNOWN::");

        private final String _string;
        private final String _lowerCase;
        private final byte[] _bytes;
        private final byte[] _bytesColonSpace;
        private final ByteBuffer _buffer;

        HttpHeader(String s) {
            _string = s;
            _lowerCase = s.toLowerCase();
            _bytes = s.getBytes();
            _bytesColonSpace = (s + ": ").getBytes();
            _buffer = ByteBuffer.wrap(_bytes);
        }

        public String lowerCaseName() {
            return _lowerCase;
        }

        public ByteBuffer toBuffer() {
            return _buffer.asReadOnlyBuffer();
        }

        public byte[] getBytes() {
            return _bytes;
        }

        public byte[] getBytesColonSpace() {
            return _bytesColonSpace;
        }

        public boolean is(String s) {
            return _string.equalsIgnoreCase(s);
        }

        public String asString() {
            return _string;
        }

        @Override
        public String toString() {
            return _string;
        }
    }

    @Override
    public void init() throws ServletException {
        ServletConfig config = getServletConfig();

        _preserveHost = Boolean.parseBoolean(config.getInitParameter("preserveHost"));
        _hostHeader = config.getInitParameter("hostHeader");
        _viaHost = config.getInitParameter("viaHost");
        if(_viaHost == null) {
            _viaHost = viaHost();
        }

        // Call super
        super.init();
    }

    public String getViaHost() {
        return _viaHost;
    }

    private static String viaHost() {
        try {
            return InetAddress.getLocalHost().getHostName();
        }
        catch(UnknownHostException x) {
            return "localhost";
        }
    }

    protected void copyRequestHeaders(HttpServletRequest clientRequest, HttpRequest.Builder proxyRequest) {
        // First clear possibly existing headers, as we are going to copy those from the client request.
        proxyRequest.clearHeaders();

        Set<String> headersToRemove = findConnectionHeaders(clientRequest);

        for(Enumeration<String> headerNames = clientRequest.getHeaderNames(); headerNames.hasMoreElements(); ) {
            String headerName = headerNames.nextElement();
            String lowerHeaderName = headerName.toLowerCase(Locale.ENGLISH);

            if(HttpHeader.HOST.is(headerName) && !_preserveHost) {
                continue;
            }

            // Remove hop-by-hop headers.
            if(HOP_HEADERS.contains(lowerHeaderName)) {
                continue;
            }
            if(headersToRemove != null && headersToRemove.contains(lowerHeaderName)) {
                continue;
            }

            for(Enumeration<String> headerValues = clientRequest.getHeaders(headerName); headerValues.hasMoreElements(); ) {
                String headerValue = headerValues.nextElement();
                if(headerValue != null) {
                    proxyRequest.putHeaders(headerName, headerValue);
                }
            }
        }

        // Force the Host header if configured
        if(_hostHeader != null) {
            proxyRequest.putHeaders(HttpHeader.HOST.asString(), _hostHeader);
        }
    }

    protected Set<String> findConnectionHeaders(HttpServletRequest clientRequest) {
        // Any header listed by the Connection header must be removed:
        // http://tools.ietf.org/html/rfc7230#section-6.1.
        Set<String> hopHeaders = null;
        Enumeration<String> connectionHeaders = clientRequest.getHeaders(HttpHeader.CONNECTION.asString());
        while(connectionHeaders.hasMoreElements()) {
            String value = connectionHeaders.nextElement();
            String[] values = value.split(",");
            for(String name : values) {
                name = name.trim().toLowerCase(Locale.ENGLISH);
                if(hopHeaders == null) {
                    hopHeaders = new HashSet<>();
                }
                hopHeaders.add(name);
            }
        }
        return hopHeaders;
    }

    protected void addProxyHeaders(HttpServletRequest clientRequest, HttpRequest.Builder proxyRequest) {
        addViaHeader(proxyRequest);
        addXForwardedHeaders(clientRequest, proxyRequest);
    }

    protected void addViaHeader(HttpRequest.Builder proxyRequest) {
        proxyRequest.putHeaders(HttpHeader.VIA.asString(), "http/1.1 " + getViaHost());
    }

    protected void addXForwardedHeaders(HttpServletRequest clientRequest, HttpRequest.Builder proxyRequest) {
        proxyRequest.putHeaders(HttpHeader.X_FORWARDED_FOR.asString(), clientRequest.getRemoteAddr());
        proxyRequest.putHeaders(HttpHeader.X_FORWARDED_PROTO.asString(), clientRequest.getScheme());
        proxyRequest.putHeaders(HttpHeader.X_FORWARDED_HOST.asString(), clientRequest.getHeader(HttpHeader.HOST.asString()));
        proxyRequest.putHeaders(HttpHeader.X_FORWARDED_SERVER.asString(), clientRequest.getLocalName());
    }

    protected boolean hasContent(HttpServletRequest clientRequest) {
        return clientRequest.getContentLength() > 0 ||
                clientRequest.getContentType() != null ||
                clientRequest.getHeader(HttpHeader.TRANSFER_ENCODING.asString()) != null;
    }


}
