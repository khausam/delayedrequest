/*
This file is part of DelayedRequest.

DelayedRequest is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

DelayedRequest is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with DelayedRequest. If not, see <https://www.gnu.org/licenses/>.
 */
package com.vaxem.delayedrequest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.time.Clock;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.cloud.tasks.v2.*;
import com.google.protobuf.ByteString;
import com.google.protobuf.Timestamp;

// Config files: https://cloud.google.com/appengine/docs/standard/java/configuration-files

public class CreateDelayedServlet extends AbstractProxyServlet {

    public static final String PROJECT_ID = "delayedrequest";
    public static final String LOCATION_ID = "us-central1";
    public static final String QUEUE_ID = "queue1";
    public static final String QUEUE_PATH = QueueName.of(PROJECT_ID, LOCATION_ID, QUEUE_ID).toString();

    private static final String URL_PARAM = "url";
    private static final String DELAY_PARAM = "delay";
    private static final String REQUEST_ID_PREFIX = "Request ID:"; // This must match the value in FCMHelper in the Pettracker project.


    // https://stackoverflow.com/questions/13592236/parse-a-uri-string-into-name-value-collection
    private static Map<String, List<String>> splitQuery(String queryString) throws UnsupportedEncodingException {
        final Map<String, List<String>> query_pairs = new LinkedHashMap<String, List<String>>();
        final String[] pairs = queryString.split("&");
        for (String pair : pairs) {
            final int idx = pair.indexOf("=");
            final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
            if (!query_pairs.containsKey(key)) {
                query_pairs.put(key, new LinkedList<String>());
            }
            final String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
            query_pairs.get(key).add(value);
        }
        return query_pairs;
    }

    private static String getParameter(Map<String, List<String>> queryMap, String paramName) {
        String val = null;
        if(queryMap.containsKey(paramName)) {
            List<String> listVal = queryMap.get(paramName);
            if(listVal != null && !listVal.isEmpty()) {
                val = listVal.get(0);
            }
        }

        return val;
    }

    @Override
    protected void service(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        try {
            ByteString byteString = ByteString.readFrom(request.getInputStream());
            String queryString = request.getQueryString();
            Map<String, List<String>> queryMap = splitQuery(queryString);

            String urlParam = getParameter(queryMap, URL_PARAM);
            if(urlParam != null && !urlParam.isEmpty()) {
                String decodedUrl = URLDecoder.decode(urlParam);

                String delayParam = getParameter(queryMap, DELAY_PARAM);
                if(delayParam != null && !delayParam.isEmpty()) {
                    try {
                        int delay = Integer.parseInt(delayParam);

                        HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder();

                        // Request method
                        httpRequestBuilder.setHttpMethod(HttpMethod.valueOf(request.getMethod()));

                        // Url
                        httpRequestBuilder.setUrl(decodedUrl);

                        // Headers
                        copyRequestHeaders(request, httpRequestBuilder);
                        addProxyHeaders(request, httpRequestBuilder);

                        // Body
                        httpRequestBuilder.setBody(byteString);

                        // Create the task
                        CloudTasksClient client = CloudTasksClient.create();
                        Task.Builder taskBuilder = Task.newBuilder().setHttpRequest(httpRequestBuilder.build());
                        taskBuilder.setScheduleTime(Timestamp.newBuilder().setSeconds(Instant.now(Clock.systemUTC()).plusSeconds(delay).getEpochSecond()));
                        Task task = client.createTask(QUEUE_PATH, taskBuilder.build());

                        String name = task.getName();
                        response.getWriter().print(REQUEST_ID_PREFIX + name.substring(name.lastIndexOf("/") + 1));

                        // Cleanup
                        client.shutdown();
                        client.close();
                    }
                    catch(NumberFormatException nfex) {
                        response.getWriter().println("Parameter 'delay' is formatted incorrectly. [Format: only digits]");
                    }
                }
                else {
                    response.getWriter().println("Required parameter 'delay' is missing. [Description: delay in seconds.]");
                }
            }
            else {
                response.getWriter().println("Required parameter 'url' is missing. [Description: urlencoded target url.]");
            }
        }
        catch(Exception ex) {
            response.getWriter().println("Error: " + ex.getLocalizedMessage());
            throw ex;
        }
    }
}
