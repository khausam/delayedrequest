/*
This file is part of DelayedRequest.

DelayedRequest is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

DelayedRequest is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with DelayedRequest. If not, see <https://www.gnu.org/licenses/>.
 */
package com.vaxem.delayedrequest;

import com.google.protobuf.ByteString;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;

public class LogServlet extends HttpServlet {
    @Override
    protected void service(final HttpServletRequest request, final HttpServletResponse response) throws IOException {

        // Request method (verb)
        System.out.println("Method:" + request.getMethod());

        // Headers
        for(Enumeration<String> headerNames = request.getHeaderNames(); headerNames.hasMoreElements(); ) {
            String headerName = headerNames.nextElement();

            for(Enumeration<String> headerValues = request.getHeaders(headerName); headerValues.hasMoreElements(); ) {
                String headerValue = headerValues.nextElement();
                System.out.println(headerName + ":" + headerValue);
            }
        }

        // Body
        System.out.println("Body:" + ByteString.readFrom(request.getInputStream()).toStringUtf8());
    }
}
